author:            per dahlstroem
summary:           Gitlab pages
id:                gitlab_pages
categories:        static page
environments:      git, gitlab
status:            beta
feedback link:     https://gitlab.com/npes/claat-generator/issues
analytics account: 0

# Gitlab pages

## Introduction

The purpose of this guide is to give lecturers and students a step by step guide to publicise material hosted on git lab on a web side.

### Audience

* Students on the IT Technology education  
* Lectureres at UCL  

### Prerequisites

This guide assumes that the reader has a GitLab account.

The guide assumes that the reader is familiar with the terms and preferably has used: 

    gitLab
    (gitlab pages)  
    gitLab account  
    repository  
    project  
    group  
    git clone  
    git add .  
    git commit  
    git push   

The terms in parenthesis do not need prior experience.

## Further reading

