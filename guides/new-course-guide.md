author:            Morten Bo Nielsen - Edited also by Per Dahlstroem
summary:           A guide on how to use the semester course template
id:                new-course-guide
categories:        New course repo on gitlab
environments:      Gitlab and git projects
status:            wip
feedback link:     pda@ucl.dk
analytics account: Always 0

# Guide on how to create a new semester course plan on GitLab for GitLab Pages


## Prerequisites

This guide assumes that the reader has a GitLab account.

The guide assumes that the reader is familiar with the terms and preferably has used: 

    gitLab
    (gitlab pages)  
    gitLab account  
    repository  
    project  
    group  
    git clone  
    git add .  
    git commit  
    git push   

The terms in parenthesis do not need prior experience.

## Download the empty course template by exporting it

1. Go to [https://gitlab.com/EAL-ITT/course-template/edit](https://gitlab.com/EAL-ITT/course-template/edit)
2. Go to `Settings -> General. Scroll down to: Advanced. Click: Expand  -> Click: Export project`
3. Wait for an email from GitLab. It can take several minutes before it arrives.
2. Download the .tgz file using the link received in the email, or use the download link on the current page.

Some comments to the exporting:

Exporting the empty semester course template is to ensure that the newest template version is used, opposed to using a previously "filled out" semester course plan. Forking doesn't work due to namespace issues, and general project naming. So, the recommendation is to not export e.g. an old semester course/previous course plan.

The exporting requires people to have `maintainer` rights. <!--Something that needs to be looked at.-->

<!--This may be automated using the [import/export api](https://docs.gitlab.com/ee/api/project_import_export.html). It could be put as part of CI where this guide is build.-->

## Create a new project for the new course plan  

Navigate to [https://gitlab.com/EAL-ITT](https://gitlab.com/EAL-ITT)

1. Create a new project. e.g using the '+' in the top bar or click blue the New Project button.
2. Click: `Import project`
3. Click: `Gitlab export`
2. Set name to e.g. `22s-itt2-networking`
5. Set project URL in dropdown box, e.g. `EAL-ITT` or UCL-PBa-ITS.
6. Click: `Choose File`, and choose the `.tgz` file that was downloaded previously.
7. Click: `Import project`

<!--Historic comments:

* Whatever we usually did with default branch has now been automated.
* The part where the import ended up in the wrong namespace due to a [gitlab bug](https://gitlab.com/gitlab-org/gitlab-ce/issues/52951) has been fixed.-->

## Initial update of the project

<!--Directly in GitLab do these repository settings:  -->

1. Navigate to the new project in: [https://gitlab.com/EAL-ITT](https://gitlab.com/EAL-ITT)

<!--* update project description in the repository, `settings -> general`???-->

2. It is highly recommended to `git clone` the repository, and make the following edits locally, and then push them.  

<!--File(?) that need updating may be updated automatically(?).-->
3.  Update the `project-info` file. It is written in the `project-info` file as comments what to update and the texts syntax.  

    Here is an example (2022S) filled out project.info file:
  
        REPONAME=22s-itt2-networking  
        REPOSLUG=22S-ITT2-NETWORKING  
        REPOSLUG_US=22S_ITT2_NETWORKING  
        TITLE="22S ITT2 NETWORK"  
        GROUPNAME="EAL-ITT"  

4.  Run the bash script `init_course.sh`  
This script will replace all occurences of placeholder values with the ones from the `project-info` file.  
Hint: Run the script by doubble clicking it.  

5. Commit and push repository:  

       git add .  
       git commit -a -m "initial project update"  
       git push   

<!--Historic comments:

* Page visibility is now set correctly
* `course template` placeholder will be automatically replaced by the content of `project-info`. Yes, we aim for userfriendlines.-->

## Build the pages for the website

Building the pages and the website is done automatically by GitLab on push, so no action is needed.  

Note that building and deplyoing pages for the first time can take up to 5-10 min.

<!--## Connect the origin repo(?) to new repo

This is an optional step, and will enable easy update of the repo with new features from the original repo.

1. `git remote add template-upstream git@gitlab.com:EAL-ITT/course-template.git`
2. `git pull template-upstream master`

  Use with caution(?), since updates from template upstream may break your specific setup.-->
