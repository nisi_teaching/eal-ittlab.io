# eal-itt.gitlab.io layout

Layout created with [https://materializecss.com](https://materializecss.com)

These files are used to develop the layout before addidng them to the jinja 2 template  

It is recommended to edit them locally in your favourite editor and using something like [vscode-live-server](https://github.com/ritwickdey/vscode-live-server/blob/master/docs/faqs.md)
