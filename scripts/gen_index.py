#!/usr/bin/env python3

import yaml
from jinja2 import Template, FileSystemLoader, Environment


def gen_index( repos, template_file = "index.html.j2" ):
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    template = env.get_template( template_file)

    html = template.render(repos)

    return html

def read_yml( filename="repos.yml"):
    stream = open(filename, "r")
    repos = yaml.safe_load(stream)
    return repos



if __name__ == "__main__":
    repos = read_yml()

    print( gen_index(repos ))
